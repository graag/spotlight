# 
#  Copyright (c) Dariusz Biskup
#  
#  This file is part of Spotlight
# 
#  Spotlight is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as 
#  published by the Free Software Foundation; either version 3 of 
#  the License, or (at your option) any later version.
#  
#  Spotlight is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
# 
#  You should have received a copy of the GNU General Public License
#  along with this program.  If not, see <http://www.gnu.org/licenses/>
#
import xbmc


class Page:
        
    def __init__(self, start = 0, offset = 0, max_items = 0, identifier = '', infinite=True):
        self.start = start
        self.offset = offset
        self.max_items = max_items
        self.identifier = identifier
        self.inf_flag = infinite
        self.fix_offset()
        
    def next(self):
        new_start = self.start + self.offset
        new_offset = self.offset
        
        if not self.has_next():
            return None
        
        if new_start + self.offset > self.max_items and self.max_items != 0:
            new_offset = self.max_items - new_start
        
        return Page(new_start, new_offset, self.max_items, self.identifier, self.inf_flag)
    
    def has_next(self):
        
        return (self.start + self.offset < self.max_items or self.max_items == 0) and not self.is_infinite()
            
    def current_range(self):
        
        return range(self.start, self.start + self.offset)
    
    def is_infinite(self):
        
        xbmc.log("Spotlight: Page is infinite = %s" % self.inf_flag, level=xbmc.LOGDEBUG)
        return self.inf_flag
    
    def fix_offset(self):
        if self.start + self.offset > self.max_items and self.max_items > 0:
            self.offset = self.max_items - self.start

    def cache_key(self):
        return '%s %s %s %s' % (self.start, self.offset, self.max_items, self.identifier)

    def with_updated_max_items(self, max_items):
        xbmc.log("Spotlight: Update max items (current, new) = (%s, %s)" % (self.max_items, max_items), level=xbmc.LOGDEBUG)
        if self.max_items is None or self.max_items == 0:
            xbmc.log("Spotlight: New max item count = %s" % max_items, level=xbmc.LOGDEBUG)
            page = Page(self.start, self.offset, max_items, self.identifier, self.inf_flag)
            page.fix_offset()
        else:
            page = self
        return page

    @staticmethod
    def from_obj(obj):
        xbmc.log("Creating page from obj:%s" % obj)
        page = Page()
        page.start = obj.get('start')
        page.offset = obj.get('offset')
        page.max_items = obj.get('max_items')
        page.identifier = obj.get('identifier')
        page.inf_flag = obj.get('inf_flag')
        page.fix_offset()
        xbmc.log("Created page:%s" % page)

        return page

    @staticmethod        
    def infinite(identifier = ''):
        
        return Page(0, 0, 0, identifier, infinite=True)


        
